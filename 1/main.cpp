#include <iostream>

using namespace std;

int get_row_size(int begin_index, int end_index);
bool set_if_greater(int* current_max, int new_possible_value);

int main() {
    int flowers_count;
    cin >> flowers_count;
    if (flowers_count == 1) {
        cout << "1 1";
        return 0;
    }
    if (flowers_count == 2) {
        cout << "1 2";
        return 0;
    }
    int beginning_flower_index_of_good_row = 0;
    int* max_good_row_length = new int;
    *max_good_row_length = -1;
    int beginning_flower_index_of_max_good_row = beginning_flower_index_of_good_row;
    int cached_flower_type, second_cached_flower_type, current_flower_type;
    cin >> cached_flower_type;
    cin >> second_cached_flower_type;
    for (int i = 2; i < flowers_count; i++) {
        cin >> current_flower_type;
        if (cached_flower_type == second_cached_flower_type && second_cached_flower_type == current_flower_type) {
            int broken_good_row_length = get_row_size(beginning_flower_index_of_good_row, i - 1);
            bool is_max_good_row_for_now = set_if_greater(max_good_row_length, broken_good_row_length);
            if (is_max_good_row_for_now) {
                beginning_flower_index_of_max_good_row = beginning_flower_index_of_good_row;
            }
            beginning_flower_index_of_good_row = i - 1;
        }
        // cache
        cached_flower_type = second_cached_flower_type;
        second_cached_flower_type = current_flower_type;
    }
    int till_the_end_good_row_length = get_row_size(beginning_flower_index_of_good_row, flowers_count - 1);
    bool is_max_good_row_for_now = set_if_greater(max_good_row_length, till_the_end_good_row_length);
    if (is_max_good_row_for_now) {
        beginning_flower_index_of_max_good_row = beginning_flower_index_of_good_row;
    }
    int last_max_good_row_flower_index = beginning_flower_index_of_max_good_row + *max_good_row_length - 1;
    cout << beginning_flower_index_of_max_good_row + 1 << " " << last_max_good_row_flower_index + 1;
    return 0;
}

int get_row_size(int begin_index, int end_index) {
    return end_index - begin_index + 1;
}

bool set_if_greater(int* current_max, int new_possible_value) {
    if (new_possible_value > *current_max) {
        *current_max = new_possible_value;
        return true;
    }
    return false;
}