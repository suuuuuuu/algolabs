#include <istream>
#include <list>
#include <queue>
#include <fstream>
#include <optional>

using namespace std;

uint32_t read_commands_count(istream& data_flow) {
    string line;
    getline(data_flow, line);
    return stoi(line);
}

enum CommandType {
    PUSH_COMMON, PUSH_VIP, POP
};

pair<CommandType, optional<uint32_t>> read_command(istream& data_flow) {
    string line;
    getline(data_flow, line);
    if (line[0] == '+') {
        return pair(CommandType::PUSH_COMMON,
                    stoi(line.substr(2)));
    }
    if (line[0] == '*') {
        return pair(CommandType::PUSH_VIP,
                    stoi(line.substr(2)));
    }
    if (line[0] == '-') {
        return pair(CommandType::POP, nullopt);
    }
    throw invalid_argument("Wrong data_flow element");
}

int main() {
    list<uint32_t> queue_first_part;
    list<uint32_t> queue_second_part;
    bool is_count_even = true;

    ifstream data_flow("input.txt");
    uint32_t commands_count = read_commands_count(data_flow);
    ofstream answers_output("output.txt");
    for (size_t i = 0; i < commands_count; i++, is_count_even = !is_count_even) {
        pair<CommandType, optional<uint32_t>> command_data = read_command(data_flow);
        auto command = command_data.first;
        auto goblin_id_opt = command_data.second;
        switch (command) {
            case CommandType::PUSH_COMMON: {
                queue_second_part.push_back(goblin_id_opt.value());
                if (is_count_even) {
                    queue_first_part.push_back(queue_second_part.front());
                    queue_second_part.pop_front();
                }
                break;
            }
            case CommandType::PUSH_VIP: {
                if (is_count_even) {
                    queue_first_part.push_back(goblin_id_opt.value());
                } else {
                    queue_second_part.push_front(goblin_id_opt.value());
                }
                break;
            }
            case CommandType::POP: {
                answers_output << queue_first_part.front() << endl;
                queue_first_part.pop_front();
                if (is_count_even) {
                    queue_first_part.push_back(queue_second_part.front());
                    queue_second_part.pop_front();
                }
                break;
            }
        }
    }

    data_flow.close();
    answers_output.close();
    return 0;
}