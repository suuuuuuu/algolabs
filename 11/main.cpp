#include <iostream>
#include <vector>
#include <list>
#include <fstream>
#include <stack>
#include <queue>
#include <unordered_map>
#include <set>
#include <optional>

using namespace std;

uint32_t r_len(const pair<uint32_t, uint32_t>& p) {
    return p.second - p.first;
}

auto m_cmp = [](const pair<uint32_t, uint32_t>& a, const pair<uint32_t, uint32_t>& b) {
    auto a_length = r_len(a);
    auto b_length = r_len(b);
    if (a_length == b_length) {
        return a.first > b.first;
    }
    return a_length > b_length; // desc
};

enum Command {
    COMMAND_ALLOCATE, COMMAND_FREE
};

enum RangeType {
    RANGE_FREE, RANGE_FILLED
};

pair<Command, uint32_t> read_next_command(istream& input) {
    string line;
    getline(input, line);
    if (line[0] == '-') {
        return pair(Command::COMMAND_FREE, stoi(line.substr(1)));
    } else {
        return pair(Command::COMMAND_ALLOCATE, stoi(line));
    }
}

int main() {
    set<pair<uint32_t, uint32_t>, decltype(m_cmp)> free_ranges_sorted_by_size(m_cmp);
    unordered_map<uint32_t, optional<uint32_t>> right_filled_neighbour_by_free_begin;
    unordered_map<uint32_t, pair<optional<pair<RangeType, uint32_t>>, optional<pair<RangeType, uint32_t>>>> filled_ranges;
    uint32_t capacity, query_count;

    ifstream data_flow("input.txt");
    ofstream output("output.txt");

    data_flow >> capacity >> query_count;
    free_ranges_sorted_by_size.insert(pair(0, capacity - 1));
    for (uint32_t query_number = 1; query_number <= query_count; query_number++) {
        auto [command, command_value] = read_next_command(data_flow);
        switch (command) {
            case COMMAND_ALLOCATE: {
                auto free_range_it = free_ranges_sorted_by_size.begin();
                auto free_range = *free_range_it;
                auto free_len = r_len(free_range);
                if (free_len < command_value) {
                    output << "-1" << endl;
                    continue;
                }
                free_ranges_sorted_by_size.erase(free_range_it);
                bool has_right_neighbour = free_len == command_value;
                if (!has_right_neighbour) {
                    auto new_free = pair(free_range.first + command_value, free_range.second);
                    free_ranges_sorted_by_size.insert(new_free);
                    auto right_neighbour_before = right_filled_neighbour_by_free_begin[free_range.first];
                    right_filled_neighbour_by_free_begin.erase(free_range.first);
                    right_filled_neighbour_by_free_begin[new_free.first] = right_neighbour_before;
                } else {
                    auto right_neighbour_opt = right_filled_neighbour_by_free_begin[free_range.first];
                    if (right_neighbour_opt.has_value()) {
                        auto right_neighbour = right_neighbour_opt.value();
                        filled_ranges[right_neighbour].first = optional(pair(RangeType::RANGE_FILLED, query_number));
                    }
                }
                break;
            }
            case COMMAND_FREE: {
                auto [left_range_info_opt, right_range_info_opt] = filled_ranges[command_value];
                auto new_free_begin = pair()
                if (left_range_info_opt.has_value()) {
                    auto left_range_info = left_range_info_opt.value();
                    switch (left_range_info.first) {
                        case RANGE_FREE: {

                            break;
                        }
                        case RANGE_FILLED: {
                            break;
                        }
                    }

                }
                if (right_range_info_opt.has_value()) {
                    auto right_range_info = right_range_info_opt.value();

                }
                break;
            }
        }
    }

    data_flow.close();
    output.close();

    return 0;
}