#include <fstream>
#include <set>
#include <list>
#include <queue>
#include <iostream>

using namespace std;

void read_nums_count_and_window_width(istream& input, uint32_t& nums_count, uint32_t& window_width) {
    string line;
    getline(input, line);
    size_t delim_index = line.find(" ");
    nums_count = stoi(line.substr(0, delim_index));
    window_width = stoi(line.substr(delim_index + 1));
}

int32_t read_next_value(istream& input) {
    int32_t read;
    input >> read;
    return read;
}

void print_next_min(ostream& output, multiset<int32_t>& window_sorted) {
    output << *window_sorted.begin() << " ";
}

int main() {
    multiset<int32_t> window_sorted;
    queue<int32_t> window_remove_queue;
    uint32_t nums_count, window_width;

    ifstream input("input.txt");
    read_nums_count_and_window_width(input, nums_count, window_width);
    ofstream output("output.txt");

    // fill window
    for (uint32_t i = 0; i < window_width; i++) {
        auto next = read_next_value(input);
        window_sorted.insert(next);
        window_remove_queue.push(next);
    }
    print_next_min(output, window_sorted);
    for (uint32_t i = 0;
            i < nums_count - window_width;
            i++, print_next_min(output, window_sorted)) {
        auto to_remove = window_remove_queue.front();
        window_sorted.erase(window_sorted.find(to_remove));
        window_remove_queue.pop();
        auto next = read_next_value(input);
        window_sorted.insert(next);
        window_remove_queue.push(next);
    }

    input.close();
    output.close();
    return 0;
}