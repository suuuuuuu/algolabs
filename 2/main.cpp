#include <iostream>
#include <vector>
#include <cctype>
#include <list>
#include <optional>
#include <stack>

using namespace std;


enum ZooObjectType {
    ANIMAL, TRAP
};

class ZooObject {
public:
    enum ZooObjectType type;
    char id_in_lower_case;

    /**
     * @throws std:invalid_argument if id is not alphabetic
     */
    ZooObject(ZooObjectType type, char id_in_any_case) {
        if (isalpha(id_in_any_case) == 0) {
            throw std::invalid_argument("received not letter as id");
        }
        this->id_in_lower_case = tolower(id_in_any_case);
        this->type = type;
    }

    bool is_trap_animal_pair_with(ZooObject* zooObject) {
        return this->id_in_lower_case == zooObject->id_in_lower_case &&
               (this->type == ANIMAL && zooObject->type == TRAP ||
                zooObject->type == ANIMAL && this->type == TRAP);
    }
};

ZooObject init_zoo_object_if_animal_is_lower_trap_is_upper_case(char id_in_case);

optional<ZooObject> read_next_zoo_object_from_console();

// (index + 1) + size_before() if need more space
void insert_to_vector_index_with_resize_if_needed(vector<size_t>* vec, size_t index, size_t element);

void increase_corresponding_zoo_index(ZooObjectType* zoo_object_type,
                                      size_t* trap_index, size_t* animal_index);

void push_to_not_paired_zoo_objects(stack<pair<size_t, ZooObject>>* not_paired_zoo_objects,
                                    ZooObject* zoo_object,
                                    size_t* trap_index, size_t* animal_index);

int main() {
    // needed data initialized:
    // - stack of not paired with indexes
    // - array of animal_indexes using trap_indexes as array indexes with dynamic capacity ((index + 1) + size_before() if need more space)
    // - current trap index
    // - current animal index
    //
    // algo:
    // 1) read char
    // 2) parse to ZooObject
    // 3) check last stack element on pair
    // 4.1) if they are, we got pair, so can remove it
    // 4.1.1.1) if it is a trap in stack then pop it,
    // 4.1.1.2) add record (containing also animal index) to array on it's index
    // 4.1.2.1) else pop an animal
    // 4.1.2.2) add record about animal index to array on trap index
    // 4.2) if aren't, add to stack with current trap / animal index
    // 5) increase current trap / animal index
    // 6.1) if haven't reach end of line, jump to 1)
    // 6.2) if reached - continue
    // 7.1) if stack is not empty, print "Impossible"
    // 7.2) if stack is empty, print "Possible" and array content

    // needed data initialized:
    // - stack of not paired with indexes
    stack<pair<size_t, ZooObject>> not_paired_zoo_objects;
    // - array of animal_indexes using trap_indexes as array indexes with dynamic capacity ((index + 1) + size_before() if need more space)
    vector<size_t> trap_to_animal_index;
    // - current trap index
    size_t current_trap_index = 0;
    // - current animal index
    size_t current_animal_index = 0;

    // algo:
    // 1) read char
    // 2) parse to ZooObject
    // 6.1) if haven't reach end of line, jump to 1)
    // 6.2) if reached - continue
    optional<ZooObject> next_zoo_object;
    while((next_zoo_object = read_next_zoo_object_from_console()) != nullopt) {
        // 3) check last stack element on pair
        // 4.1) if they are, we got pair, so can remove it
        if (not_paired_zoo_objects.empty()) {
            push_to_not_paired_zoo_objects(&not_paired_zoo_objects,
                                           &next_zoo_object.value(),
                                           &current_trap_index, &current_animal_index);
            // 5) increase current trap / animal index
            increase_corresponding_zoo_index(&next_zoo_object->type, &current_trap_index, &current_animal_index);
            continue;
        }
        pair<size_t, ZooObject>* last_stack_element = &not_paired_zoo_objects.top();
        ZooObject* last_stack_not_paired = &last_stack_element->second;
        if (next_zoo_object->is_trap_animal_pair_with(&last_stack_element->second)) {
            // 4.1.1.1) if it is a trap in stack then pop it,
            // 4.1.1.2) add record (containing also animal index) to array on it's index
            not_paired_zoo_objects.pop(); // pop animal or trap
            size_t trap_index;
            size_t animal_index;
            switch (last_stack_not_paired->type) {
                case TRAP:
                    trap_index = last_stack_element->first;
                    animal_index = current_animal_index;
                    break;
                // 4.1.2.1) else pop an animal
                // 4.1.2.2) add record about animal index to array on trap index
                case ANIMAL:
                    trap_index = current_trap_index;
                    animal_index = last_stack_element->first;
                    break;
            }
            insert_to_vector_index_with_resize_if_needed(
                    &trap_to_animal_index,
                    trap_index,
                    animal_index);
        // 4.2) if aren't, add to stack with current trap / animal index
        } else {
            push_to_not_paired_zoo_objects(&not_paired_zoo_objects,
                                            &next_zoo_object.value(),
                                            &current_trap_index, &current_animal_index);
        }
        // 5) increase current trap / animal index
        increase_corresponding_zoo_index(&next_zoo_object->type, &current_trap_index, &current_animal_index);
    }
    // 7.1) if stack is not empty, print "Impossible"
    // 7.2) if stack is empty, print "Possible" and array content
    if (!not_paired_zoo_objects.empty()) {
        cout << "Impossible";
    } else {
        cout << "Possible" << endl;
        for (size_t i = 0; i < current_trap_index; i++) {
            cout << trap_to_animal_index[i] + 1 << " ";
        }
    }
    return 0;
}

void push_to_not_paired_zoo_objects(stack<pair<size_t, ZooObject>>* not_paired_zoo_objects, ZooObject* zoo_object, size_t* trap_index, size_t* animal_index) {
    size_t element_index;
    switch (zoo_object->type) {
        case TRAP:
            element_index = *trap_index;
            break;
        case ANIMAL:
            element_index = *animal_index;
            break;
    }
    not_paired_zoo_objects->push(pair(element_index, *zoo_object));
}

void increase_corresponding_zoo_index(ZooObjectType* zoo_object_type, size_t* trap_index, size_t* animal_index) {
    switch (*zoo_object_type) {
        case TRAP:
            (*trap_index)++;
            break;
        case ANIMAL:
            (*animal_index)++;
            break;
    }
}

// (index + 1) + size_before() if need more space
void insert_to_vector_index_with_resize_if_needed(vector<size_t>* vec, size_t index, size_t element) {
    if (index >= vec->size()) {
        vec->resize((index + 1) + vec->size());
    }
    (*vec)[index] = element;
}

/**
 * @throws std:invalid_argument if id is not alphabetic
 */
ZooObject init_zoo_object_if_animal_is_lower_trap_is_upper_case(char id_in_case) {
    return ZooObject(isupper(id_in_case) != 0 ? TRAP : ANIMAL,
                     id_in_case);
}

/**
 * @return nullopt if next char is NL ('\n')
 * @throws std:invalid_argument if read id is not alphabetic
 */
optional<ZooObject> read_next_zoo_object_from_console() {
    char next_char;
    cin.get(next_char);
    if (next_char == '\n') {
        return nullopt;
    }
    return optional<ZooObject>(init_zoo_object_if_animal_is_lower_trap_is_upper_case(next_char));
}
