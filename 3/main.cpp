#include <stddef.h>
#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <optional>
#include <unordered_map>
#include <list>
#include <fstream>

using namespace std;

enum LineType {
    OPENING_BLOCK,
    CLOSING_BLOCK,
    IMMEDIATE_VARIABLE_ASSIGNMENT,
    VARIABLE_TO_VARIABLE_ASSIGNMENT,
};

void print_linetype(LineType* lineType) {
    switch (*lineType) {
        case OPENING_BLOCK:
            cout << "opening block" << endl;
            break;
        case CLOSING_BLOCK:
            cout << "closing block" << endl;
            break;
        case IMMEDIATE_VARIABLE_ASSIGNMENT:
            cout << "immediate variable assignment" << endl;
            break;
        case VARIABLE_TO_VARIABLE_ASSIGNMENT:
            cout << "variable to variable assignment" << endl;
            break;
    }
}

optional<size_t> try_assignment_symbol_index_getting(string* read_line) {
    optional<size_t> assignment_symbol_index = nullopt;
    for (size_t i = 0; i < read_line->length(); i++) {
        if ((*read_line)[i] == '=') {
           assignment_symbol_index = make_optional(i);
        }
    }
    return assignment_symbol_index;
}

LineType determineLineType(string* read_line) {
    if ((*read_line)[0] == '{') {
        return OPENING_BLOCK;
    }
    if ((*read_line)[0] == '}') {
        return CLOSING_BLOCK;
    }
    // now only immediate variable assignment or variable to variable assignment
    size_t assignment_symbol_index = try_assignment_symbol_index_getting(read_line).value();
    char first_char_after_assignment_symbol = (*read_line)[assignment_symbol_index + 1];
    if (isalpha(first_char_after_assignment_symbol)) {
        return VARIABLE_TO_VARIABLE_ASSIGNMENT;
    } else {
        return IMMEDIATE_VARIABLE_ASSIGNMENT;
    }
}

struct NestedVariable {
    size_t nested_level; // greater == deeper
    int value;
};

pair<string, string> split_assignment(string* read_line) {
    size_t assignment_symbol_index = try_assignment_symbol_index_getting(read_line).value();
    string name = read_line->substr(0, assignment_symbol_index);
    string value = read_line->substr(assignment_symbol_index + 1);
    return pair(name, value);
}

string parse_var_was_assigned_name(string* read_line) {
    size_t assignment_symbol_index = try_assignment_symbol_index_getting(read_line).value();
    return read_line->substr(0, assignment_symbol_index);
}

pair<string, int> parse_imm_var_assignment(string* read_line) {
    pair<string, string> name_and_value = split_assignment(read_line);
    string name = name_and_value.first;
    int value = stoi(name_and_value.second);
    return pair(name , value);
}

pair<string, string> parse_var_to_var_assignment(string* read_line) {
    return split_assignment(read_line);
}

// gets 0 if var name specified, but not found
int get_value_from_assignment_line(unordered_map<string, list<NestedVariable>>* variables,
                                   string* read_line,
                                   size_t assigned_var_name_length);


int main() {
    // needed data initialized:
    // - hashmap of variables names as keys and linked lists of values in blocks with block nesting level (will be deleted on block exit in right order) as values
    // - linked list of variable names. On block exit hashmap will delete last records, maintained by block (see list of counts), in corresponded lists
    // - linked blocks variables countss variables count
    // - current block nesting level

    // algo:
    //
    // 1) read line and determine type
    // 2.1) if it is assignment, if "left" var presented in vars map, check last element of list and compare with current block nesting level
    // also for every case print assigned value
    // 2.1.1) if not even presented, push the value (imm, if imm, get from map if var, if has't - 0) to vars map list
    // and add var name to list of variables names, increase last element of blocks variables countss variables count
    // 2.1.2) if presented and from current block, set the value of last list element
    // 2.1.3) if presented and not from current block, push the value to vars map list
    // and add var name to list of variables names, increase last element of blocks variables countss variables count
    // 2.2) if it is block open, push 0 as variable count and increase current block nesting level
    // 2.3) if it is block close, pop blocks variables counts from list of variable names using linked list of blocks variables count,
    // pop the variable count, remove last entries of corresponding list in vars map and decrease current block nesting level
    // 3) check on eof
    // 3.1) if it is, end up with file
    // 3.2) if not, continue reading

    // needed data initialized:
    // - hashmap of variables names as keys and linked lists of values in blocks with block nesting level (will be deleted on block exit in right order) as values
    unordered_map<string, list<NestedVariable>> variables = unordered_map<string, list<NestedVariable>>();
    // - linked list of variable names. On block exit hashmap will delete last records, maintained by block (see list of counts), in corresponded lists
    list<string> variable_names_records = list<string>();
    // - linked blocks variables counts variables count
    list<size_t> blocks_variables_counts = list<size_t>();
    blocks_variables_counts.push_back(0);
    // - current block nesting level
    size_t current_block_nesting_level = 0;
    // - answer list - not intended to be present in alg answer
    list<int> output = list<int>();

    // algo:
    //
    // 1) read line and determine type
    fstream newfile;
    newfile.open("input.txt",ios::in); //open a file to perform read operation using file object
    string read_line;
    while (getline(newfile, read_line)) {
        LineType lineType = determineLineType(&read_line);
        switch (lineType) {
            // 2.1) if it is assignment... , if "left" var presented in vars map, check last element of list and compare with current block nesting level
            // also for every case print assigned value
            case IMMEDIATE_VARIABLE_ASSIGNMENT:
            case VARIABLE_TO_VARIABLE_ASSIGNMENT: {
                string variable_name = parse_var_was_assigned_name(&read_line);
                int value_to_assign = get_value_from_assignment_line(&variables, &read_line, variable_name.length());
                if (lineType == VARIABLE_TO_VARIABLE_ASSIGNMENT) {
                    output.push_back(value_to_assign);
                }
                // 2.1.1) if not even presented, push the value (imm, if imm, get from map if var, if has't - 0) to vars map list
                // and add var name to list of variables names, increase last element of blocks variables counts variables count
                if (variables.find(variable_name) == variables.end()) {
                    variables[variable_name] = list<NestedVariable>();
                    variables[variable_name].push_back({current_block_nesting_level, value_to_assign});
                    variable_names_records.push_back(variable_name);
                    blocks_variables_counts.back()++;
                // 2.1.2) if presented and from current block, set the value of last list element
                } else if (variables[variable_name].back().nested_level == current_block_nesting_level) {
                    variables[variable_name].back().value = value_to_assign;
                // 2.1.3) if presented and not from current block, push the value to vars map list
                // and add var name to list of variables names, increase last element of blocks variables counts variables count
                } else {
                    variables[variable_name].push_back({current_block_nesting_level, value_to_assign});
                    variable_names_records.push_back(variable_name);
                    blocks_variables_counts.back()++;
                }
                break;
            }
            // 2.2) if it is block open, push 0 as variable count and increase current block nesting level
            case OPENING_BLOCK: {
                blocks_variables_counts.push_back(0);
                current_block_nesting_level++;
                break;
            }
            // 2.3) if it is block close, pop blocks variables counts from list of variable names using linked list of blocks variables count
            // and remove last entries of corresponding list in vars map and decrease current block nesting level
            case CLOSING_BLOCK: {
                size_t block_variables_count = blocks_variables_counts.back();
                blocks_variables_counts.pop_back();
                for (size_t i = 0; i < block_variables_count; i++) {
                    string var_record_to_remove_name = variable_names_records.back();
                    variables[var_record_to_remove_name].pop_back();
                    if (variables[var_record_to_remove_name].empty()) {
                        variables.erase(var_record_to_remove_name);
                    }
                    variable_names_records.pop_back();
                }
                current_block_nesting_level--;
                break;
            }
        }
    }
    for (auto const &record : output) {
        cout << record << endl;
    }
    return 0;
}

// gets 0 if var name specified, but not found
int get_value_from_assignment_line(unordered_map<string, list<NestedVariable>>* variables, string* read_line, size_t assigned_var_name_length) {
    size_t first_char_index = (assigned_var_name_length - 1) + 1 + 1; // +1 eq sign and +1 first char
    string line_second_part = read_line->substr(first_char_index);
    // is variable
    if (isalpha(line_second_part[0])) {
        // is not presented
        if (variables->find(line_second_part) == variables->end()) {
            return 0;
        // presented
        } else {
            return (*variables)[line_second_part].back().value;
        }
    // imm value
    } else {
        return stoi(line_second_part);
    }
}