#include <iostream>

using namespace std;

void iterate(long long* r, int16_t* b, int16_t* c) {
    *r = *r * *b - *c;
    if (*r < 0) {
        *r = 0;
    }
}

int main() {
    // algo
    // 1) read r, b, c, d and k values
    // 2) get one iteration, decrease k therefore
    // 3) check increasing / decreasing condition
    // 3.1) on increase start iteration to k, if reached d - return it. Return answer if reached k day
    // 3.2) on eq return r
    // 3.3) on decrease start iteration to 0, if reached 0 - return it. Return answer if reached k day
    long long r;
    int16_t b, c, d;
    long long k;
    // 1) read r, b, c, d and k values
    cin >> r >> b >> c >> d >> k;
    // 2) get one iteration, decrease k therefore
    iterate(&r, &b, &c);
    k--;
    // 3) check increasing / decreasing condition
    if (r * b - c > r) {
        // 3.1) on increase start iteration to k, return answer
        for (long long i = 0; i < k; i++) {
            iterate(&r, &b, &c);
            if (r >= d) {
                cout << d;
                return 0;
            }
        }
        cout << r;
        return 0;
    } else if (r * b - c == r) {
        // 3.2) on eq return r
        cout << r;
        return 0;
    } else {
        // 3.3) on decrease start iteration to 0, if reached 0 - return it. Return answer if reached k day
        for (long long i = 0; i < k; i++) {
            iterate(&r, &b, &c);
            if (r == 0) {
                cout << 0;
                return 0;
            }
        }
        cout << r;
        return 0;
    }
}