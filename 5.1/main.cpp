#include <cstdint>
#include <vector>
#include <iostream>
#include <valarray>

using namespace std;


void read_stalls_and_cows_count(uint16_t& stalls_count, uint16_t& cows_count) {
    cin >> stalls_count >> cows_count;
}

void read_stalls_coordinates(vector<uint32_t>& stalls_coordinates, uint16_t& count) {
    stalls_coordinates.reserve(count);
    for (uint16_t i = 0; i < count; i++) {
        uint32_t stall_coordinate;
        cin >> stall_coordinate;
        stalls_coordinates.push_back(stall_coordinate);
    }
}

uint32_t divide_with_ceil(uint32_t number, uint16_t divide_to) {
    return (number + (divide_to - 1)) / divide_to;
}

uint32_t get_max_max_min(vector<uint32_t>& stalls_coordinates, uint16_t& cows_count) {
    uint32_t most_left_stall_coordinate = stalls_coordinates.front();
    uint32_t most_right_stall_coordinate = stalls_coordinates.back();
    uint16_t cows_in_between_count = cows_count - 2;
    uint16_t gaps_in_between_count = cows_in_between_count + 1;
    uint32_t between_corners_cows_gap_size = (most_right_stall_coordinate - most_left_stall_coordinate);
    return divide_with_ceil(between_corners_cows_gap_size, gaps_in_between_count);
}

bool can_be_placed(uint32_t& max_min, uint16_t& cows_count, vector<uint32_t>& stalls_coordinates) {
    uint16_t placed_count = 1;
    uint32_t placed_before_coordinate = stalls_coordinates.front();
    for (uint32_t next_coordinate : stalls_coordinates) {
        if (next_coordinate - placed_before_coordinate >= max_min) {
            placed_before_coordinate = next_coordinate;
            placed_count += 1;
            if (placed_count >= cows_count) {
                return true;
            }
        }
    }
    return false;
}

int main() {
    vector<uint32_t> stalls_coordinates;
    uint16_t stalls_count;
    uint16_t cows_count;

    read_stalls_and_cows_count(stalls_count, cows_count);
    read_stalls_coordinates(stalls_coordinates, stalls_count);

    uint32_t max_possible_max_min = get_max_max_min(stalls_coordinates, cows_count);

    if (cows_count == 2) {
        cout << max_possible_max_min;
        return 0;
    }

    uint32_t min_possible_max_min = 1;
    while (min_possible_max_min != max_possible_max_min) {
        uint32_t max_min_in_between = min_possible_max_min + divide_with_ceil(max_possible_max_min - min_possible_max_min, 2);
        if (can_be_placed(max_min_in_between, cows_count, stalls_coordinates)) {
            min_possible_max_min = max_min_in_between;
        } else {
            max_possible_max_min = max_min_in_between - 1;
        }
    }
    cout << min_possible_max_min;
    return 0;
}