#include <iostream>
#include <vector>
#include <list>
#include <optional>

using namespace std;

void test1(vector<uint32_t>* stalls_coordinates, uint16_t* cows_count) {
//    *stalls_coordinates = {1,5,7,11,15,20};
//    *cows_count = 3;
//    *stalls_coordinates = {1,2,3,100,1000};
//    *cows_count = 3;
//    *stalls_coordinates = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17};
//    *cows_count = 17;
//    *stalls_coordinates = {0, 5, 10, 14, 16, 20};
//    *cows_count = 4;
    *stalls_coordinates = {0, 3, 4, 5, 8};
    *cows_count = 4;
}

struct Gap {
    uint32_t size;
    size_t left_index;
    size_t right_index;

    static Gap init_from_coordinates_vector(vector<uint32_t>* coordinates, size_t* left_index, size_t* right_index) {
        return Gap {
                .size = (*coordinates)[*right_index] - (*coordinates)[*left_index],
                .left_index = *left_index,
                .right_index = *right_index,
        };
    }

    size_t stalls_inside_count() {
        return this->right_index - this->left_index - 1;
    }

    /**
     * @return true if hasn't gaps inside
     */
    bool is_single() {
        return stalls_inside_count() == 0;
    }

    void print() {
        cout << "Gap size: " << this->size << endl;
        cout << "Gap left index: " << this->left_index << endl;
        cout << "Gap right index: " << this->right_index << endl;
    }
};



struct Gap_divide_with_min_size {
    Gap left;
    Gap right;
    uint32_t min_size;

    static Gap_divide_with_min_size init(Gap left, Gap right) {
        return Gap_divide_with_min_size {
                .left = left,
                .right = right,
                .min_size = min(left.size, right.size)
        };
    }

    void print() {
        cout << "left: " << endl;
        left.print();
        cout << "right: " << endl;
        right.print();
        cout << "min_size: " << min_size << endl;
    }
};


bool operator==(const Gap& first, const Gap& second) {
    return first.left_index == second.left_index &&
           first.right_index == second.right_index;
}

bool operator==(const pair<Gap, Gap>& first, const pair<Gap, Gap>& second) {
    return first.first == second.first &&
           first.second == second.second;
}

bool operator==(const Gap_divide_with_min_size& first, const Gap_divide_with_min_size& second) {
    return first.left.left_index == second.left.left_index &&
           first.left.right_index == second.left.right_index &&
           first.right.right_index == second.right.right_index;
}

void read_stalls_and_cows_count(uint16_t* stalls_count, uint16_t* cows_count) {
    cin >> *stalls_count >> *cows_count;
}

void read_stalls_coordinates(vector<uint32_t>* stalls_coordinates, uint16_t* count) {
    stalls_coordinates->reserve(*count);
    for (uint16_t i = 0; i < *count; i++) {
        uint32_t stall_coordinate;
        cin >> stall_coordinate;
        stalls_coordinates->push_back(stall_coordinate);
    }
}

Gap init_begin_gap(vector<uint32_t>* stalls_coordinates) {
    size_t left_index = 0;
    size_t right_index = stalls_coordinates->size() - 1;
    return Gap::init_from_coordinates_vector(stalls_coordinates, &left_index, &right_index);
}

size_t* chose_max_min_delimiter(size_t* first_delimiter,
                                size_t* second_delimiter,
                                Gap* parent,
                                vector<uint32_t>* stalls_coordinates);


// divide gap by indexes - half of stalls (of gap) is on left, half - on right
// if left gap size < right gap size,
//  if left gap is not single, then call binary search for right as to_search_in
//  else (if single) return left right delimiter
// else if left gap size >= right gap size
//  do the same for fight gap
size_t binary_search_of_max_min_gaps_delimiter(Gap* to_search_in, Gap* parent, vector<uint32_t>* stalls_coordinates) {
    size_t middle_stall_coordinate_index = to_search_in->left_index + 1 + (to_search_in->stalls_inside_count() / 2);
    Gap left_gap = Gap::init_from_coordinates_vector(stalls_coordinates,
                                                     &to_search_in->left_index, &middle_stall_coordinate_index);
    Gap right_gap = Gap::init_from_coordinates_vector(stalls_coordinates,
                                                      &middle_stall_coordinate_index, &to_search_in->right_index);
    Gap left_till_parent_gap = Gap::init_from_coordinates_vector(stalls_coordinates,
                                                                 &parent->left_index, &middle_stall_coordinate_index);
    Gap right_till_parent_gap = Gap::init_from_coordinates_vector(stalls_coordinates,
                                                                  &middle_stall_coordinate_index, &parent->right_index);
    Gap* greater_gap;
    // compares in asm only once by this if else branch
    if (left_till_parent_gap.size > right_till_parent_gap.size) {
        greater_gap = &left_gap;
    } else if (left_till_parent_gap.size < right_till_parent_gap.size) {
        greater_gap = &right_gap;
    } else {
        return middle_stall_coordinate_index;
    }
    if (greater_gap->is_single()) {
        return middle_stall_coordinate_index;
    }
    size_t greater_gap_max_min_delimiter =
            binary_search_of_max_min_gaps_delimiter(greater_gap, parent, stalls_coordinates);
    return *chose_max_min_delimiter(&greater_gap_max_min_delimiter,
                                    &middle_stall_coordinate_index,
                                    parent,
                                    stalls_coordinates);
}

size_t get_min_size_of_delimited_gaps(size_t* delimiter,
                                      Gap* parent,
                                      vector<uint32_t>* stalls_coordinates) {
    size_t left_size = (*stalls_coordinates)[*delimiter] - (*stalls_coordinates)[parent->left_index];
    size_t right_size = (*stalls_coordinates)[parent->right_index] - (*stalls_coordinates)[*delimiter];
    return min(left_size, right_size);
}

size_t* chose_max_min_delimiter(size_t* first_delimiter,
                             size_t* second_delimiter,
                             Gap* parent,
                             vector<uint32_t>* stalls_coordinates) {
    size_t first_delimiter_min_gap_size = get_min_size_of_delimited_gaps(first_delimiter, parent, stalls_coordinates);
    size_t second_delimiter_min_gap_size = get_min_size_of_delimited_gaps(second_delimiter, parent, stalls_coordinates);
    if (first_delimiter_min_gap_size > second_delimiter_min_gap_size) {
        return first_delimiter;
    } else {
        return second_delimiter;
    }
}

Gap_divide_with_min_size break_gap_with_binary_max_min_delimiters_search(Gap* gap, vector<uint32_t>* stalls_coordinates) {
    size_t max_min_delimiter = binary_search_of_max_min_gaps_delimiter(gap, gap, stalls_coordinates);
    Gap left_gap = Gap::init_from_coordinates_vector(stalls_coordinates, &gap->left_index, &max_min_delimiter);
    Gap right_gap = Gap::init_from_coordinates_vector(stalls_coordinates, &max_min_delimiter, &gap->right_index);
    return Gap_divide_with_min_size::init(left_gap, right_gap);
}

pair<optional<Gap_divide_with_min_size>*, optional<Gap_divide_with_min_size>*>
get_lower_and_greater_in_order_if_has(
        optional<Gap_divide_with_min_size>* first_size_pair,
        optional<Gap_divide_with_min_size>* second_size_pair) {
    if (first_size_pair->has_value() && second_size_pair->has_value()) {
        if (first_size_pair->value().min_size > second_size_pair->value().min_size) {
            return pair(second_size_pair, first_size_pair);
        } else {
            return pair(first_size_pair, second_size_pair);
        }
    } else if (first_size_pair->has_value()) {
        return pair(first_size_pair, second_size_pair);
    } else if (second_size_pair->has_value()) {
        return pair(second_size_pair, first_size_pair);
    } else {
        return pair(first_size_pair, second_size_pair);
    }
}

void add_optionals_pair_in_sorted_list_by_max_min_pair_size(optional<Gap_divide_with_min_size>* first_min_size_pair,
                                                            optional<Gap_divide_with_min_size>* second_min_size_pair,
                                                            list<Gap_divide_with_min_size>* min_gaps_size_sorted_pairs) {
    pair<optional<Gap_divide_with_min_size>*, optional<Gap_divide_with_min_size>*> lower_and_greater_size_pairs_in_order =
            get_lower_and_greater_in_order_if_has(first_min_size_pair, second_min_size_pair);
    if (!lower_and_greater_size_pairs_in_order.first->has_value()) {
        return;
    }
    uint32_t lower_value = lower_and_greater_size_pairs_in_order.first->value().min_size;
    auto min_gaps_size_sorted_pairs_iterator = min_gaps_size_sorted_pairs->begin();
    bool inserted_lower = false;
    while (min_gaps_size_sorted_pairs_iterator != min_gaps_size_sorted_pairs->end()) {
        if (lower_value <= min_gaps_size_sorted_pairs_iterator->min_size) {
            min_gaps_size_sorted_pairs->insert(min_gaps_size_sorted_pairs_iterator,
                                               lower_and_greater_size_pairs_in_order.first->value());
            inserted_lower = true;
            break;
        }
        min_gaps_size_sorted_pairs_iterator++;
    }
    if (!inserted_lower) {
        min_gaps_size_sorted_pairs->push_back(lower_and_greater_size_pairs_in_order.first->value());
    }
    if (!lower_and_greater_size_pairs_in_order.second->has_value()) {
        return;
    }
    uint32_t greater_value = lower_and_greater_size_pairs_in_order.second->value().min_size;
    bool inserted_greater = false;
    while (min_gaps_size_sorted_pairs_iterator != min_gaps_size_sorted_pairs->end()) {
        if (greater_value <= min_gaps_size_sorted_pairs_iterator->min_size) {
            min_gaps_size_sorted_pairs->insert(min_gaps_size_sorted_pairs_iterator,
                                               lower_and_greater_size_pairs_in_order.second->value());
            inserted_greater = true;
            break;
        }
        min_gaps_size_sorted_pairs_iterator++;
    }
    if (!inserted_greater) {
        min_gaps_size_sorted_pairs->push_back(lower_and_greater_size_pairs_in_order.second->value());
    }
}

optional<Gap_divide_with_min_size> get_binary_searched_max_min_divide_if_not_single(Gap* gap, vector<uint32_t>* stalls_coordinates) {
    if (!gap->is_single()) {
        return optional(break_gap_with_binary_max_min_delimiters_search(gap, stalls_coordinates));
    } else {
        return nullopt;
    }
}

void print(list<Gap_divide_with_min_size> const &list)
{
    cout << "list: ";
    for (auto const &i: list) {
        cout << i.min_size << ", ";
    }
    cout << endl;
}

int main() {
    // data:
    // - stall's array of coordinates in right order
    vector<uint32_t> stalls_coordinates;
    // - linked list of sorted pairs of gaps by min gaps size in pairs (min value link and pair link)
    list<Gap_divide_with_min_size> min_gaps_size_sorted_pairs;
    // - cows count left to put in stalls
    uint16_t cows_count_left;
    // - max min
    uint32_t max_min;

    // algo:
    // 1) read cows_count and stalls_coordinates
    // 2) take boundary values as gap boundaries
    // 2.1) check for start cows_count == 2
    // 2.2) cows_count -= 2
    // 3) break gap with binary search algo
    // 3.1) add this gaps pair to list of min_gaps_size_sorted_pairs (to start algo in cycle)
    // Cycle (works until cows_count = 0):
    // 4) pop last gaps pair (max value) of min_gaps_size_sorted_pairs
    // 5) update max_min with min gaps size in pair value
    // 5.1) decrease cows_count
    // 6) break both (if not single) gaps and
    // add to min_gaps_size_sorted_pairs
    // 7) jump to while condition (before 4)
    // 8) print max_min as answer

    // algo:
    // 1) read cows_count and stalls_coordinates
    uint16_t stalls_coordinates_count;
    read_stalls_and_cows_count(&stalls_coordinates_count, &cows_count_left);
    read_stalls_coordinates(&stalls_coordinates, &stalls_coordinates_count);
    test1(&stalls_coordinates, &cows_count_left);
    max_min = stalls_coordinates[stalls_coordinates.size() - 1] - stalls_coordinates[0];
    // 2) take boundary values as gap boundaries
    Gap begin_gap = init_begin_gap(&stalls_coordinates);
    // 2.1) check for start cows_count == 2
    if (cows_count_left == 2) {
        cout << begin_gap.size;
        return 0;
    }
    // 2.2) cows_count -= 2
    cows_count_left -= 2;
    // 3) break gap with binary search algo
    Gap_divide_with_min_size left_right_gaps = break_gap_with_binary_max_min_delimiters_search(&begin_gap, &stalls_coordinates);
    // 3.1) add this gaps pair to list of min_gaps_size_sorted_pairs (to start algo in cycle)
    min_gaps_size_sorted_pairs.push_back(left_right_gaps);
    // Cycle (works until cows_count = 0):
    while (cows_count_left != 0) {
        // 4) pop last gaps pair (max value) of min_gaps_size_sorted_pairs
//        print(min_gaps_size_sorted_pairs);
        Gap_divide_with_min_size max_min_size_pair_data = min_gaps_size_sorted_pairs.back();
        min_gaps_size_sorted_pairs.pop_back();
        // 5) update max_min with min gaps size in pair value
        max_min = min(max_min_size_pair_data.min_size, max_min);
        // 5.1) decrease cows_count
        cows_count_left--;
        // 6) break both (if not single) gaps and
        // add to min_gaps_size_sorted_pairs
        optional<Gap_divide_with_min_size> left_gaps = get_binary_searched_max_min_divide_if_not_single(
                &max_min_size_pair_data.left,
                &stalls_coordinates);
        optional<Gap_divide_with_min_size> right_gaps = get_binary_searched_max_min_divide_if_not_single(
                &max_min_size_pair_data.right,
                &stalls_coordinates);
        add_optionals_pair_in_sorted_list_by_max_min_pair_size(&left_gaps, &right_gaps, &min_gaps_size_sorted_pairs);
        // 7) jump to while condition (before 4)
    }
    // 8) print max_min as answer
    cout << max_min;
    return 0;
}