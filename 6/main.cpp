#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

int cmp_number_parts_ok(string& first, string& second);

bool cmp_numbers_parts(string& first, string& second) {
    return cmp_number_parts_ok(first, second) > 0;
}

int cmp_number_parts_ok(string& first, string& second) {
    string* base = &first;
    string* container = &second;
    int changed_coefficient = 1;
    if (first.length() > second.length()) {
        base = &second;
        container = &first;
        changed_coefficient = -1;
    }
    auto container_iterator = container->begin();
    while (true) {
        auto base_iterator = base->begin();
        while (base_iterator != base->end() && container_iterator != container->end()) {
            if (*container_iterator == *base_iterator) {
                base_iterator++;
                container_iterator++;
                continue;
            } else if (*container_iterator < *base_iterator) {
                return 1 * changed_coefficient;
            } else {
                return -1 * changed_coefficient;
            }
        }
        if (container_iterator == container->end()) {
            if (base_iterator == base->end()) {
                return 0;
            } else { // (tail (base_iterator) + base from begin (tail_base)) cmp with (base from begin (full_base))
                auto full_base = base->begin();
                while (base_iterator != base->end()) {
                    if (*full_base == *base_iterator) {
                        full_base++;
                        base_iterator++;
                        continue;
                    } else if (*full_base < *base_iterator) {
                        return 1 * changed_coefficient;
                    } else {
                        return -1 * changed_coefficient;
                    }
                }
                auto base_tail_iterator = base->begin();
                while (full_base != base->end()) {
                    if (*full_base == *base_tail_iterator) {
                        full_base++;
                        base_tail_iterator++;
                        continue;
                    } else if (*full_base < *base_tail_iterator) {
                        return 1 * changed_coefficient;
                    } else {
                        return -1 * changed_coefficient;
                    }
                }
                return 0;
            }
        }
    }
}

void read_number_parts(vector<string>& number_parts, fstream& numbers_flow) {
    string next_number;
    while(numbers_flow >> next_number) {
        number_parts.push_back(next_number);
    }
}

void print_full_number(vector<string>& number_parts, ofstream& output_number_flow) {
    for (auto part : number_parts) {
        output_number_flow << part;
    }
}

int main() {
    vector<string> number_parts;

    fstream numbers_flow("number.in");
    read_number_parts(number_parts, numbers_flow);
    numbers_flow.close();

    sort(number_parts.begin(), number_parts.end(), cmp_numbers_parts);

    ofstream output_number_flow("number.out");
    print_full_number(number_parts, output_number_flow);
    output_number_flow.close();

    return 0;
}