#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

size_t get_letter_index(char& letter) {
    return letter - 'a';
}

string read_string() {
    string str;
    getline(cin, str);
    return str;
}

vector<uint32_t> read_weights() {
    size_t letters_count = 26;
    vector<uint32_t> weights;
    weights.reserve(letters_count);
    uint32_t next;
    for (size_t i = 0; i < letters_count; i++) {
        cin >> next;
        weights.push_back(next);
    }
    return weights;
}

vector<pair<char, size_t>> get_letters_with_counts(string& str) {
    vector<pair<char, size_t>> letters_with_counts;
    letters_with_counts.reserve(26);
    for (char letter = 'a'; letter <= 'z'; letter++) {
        letters_with_counts.push_back(pair(letter, 0));
    }
    for (char letter : str) {
        letters_with_counts[get_letter_index(letter)].second += 1;
    }
    return letters_with_counts;
}

int cmp_letters_by_weight(pair<char, size_t>& first, pair<char, size_t>& second, vector<uint32_t>& weights) {
    char first_letter = first.first;
    char second_letter = second.first;
    uint32_t first_weight = weights[get_letter_index(first_letter)];
    uint32_t second_weight = weights[get_letter_index(second_letter)];
    if (first_weight > second_weight) {
        return 1;
    } else if (first_weight == second_weight) {
        return 0;
    } else {
        return -1;
    }
}

bool is_greater_letter_weight_desc(pair<char, size_t>& first, pair<char, size_t>& second, vector<uint32_t>& weights) {
    return cmp_letters_by_weight(first, second, weights) > 0;
}

int main() {
    string str = read_string();
    vector<uint32_t> weights = read_weights();

    // 1) create (letter, count) for str
    // 2) filter count >= 2 to get important letters (save not important in another list)
    // and decrease it by 2 (for placing remaining in middle)
    // 3) sort it in desc order
    // 4) create beginning of the res str with letters in order
    // 5) put remaining letters from important and not important lists
    // 6) sink beginning in end of string

    // 1) create (letter, count) for str
    vector<pair<char, size_t>> letters_with_counts = get_letters_with_counts(str);
    // 2) filter count >= 2 to get important letters (save not important in another list)
    // and decrease it by 2 (for placing remaining in middle)
    vector<pair<char, size_t>> remaining_paired_letters_with_counts;
    vector<char> single_letters;
    for (pair<char, size_t> letter_with_count : letters_with_counts) {
        char letter = letter_with_count.first;
        size_t count = letter_with_count.second;
        if (count >= 2) {
            remaining_paired_letters_with_counts.push_back(pair(letter, count - 2));
        } else if (count == 1) {
            single_letters.push_back(letter);
        }
        // ignoring not presented
    }
    // 3) sort it in desc order
    sort(begin(remaining_paired_letters_with_counts),
         end(remaining_paired_letters_with_counts),
    [&weights](pair<char, size_t> &first, pair<char, size_t> &second){ return is_greater_letter_weight_desc(first, second, weights); });
    // 4) create beginning of the res str with letters in order
    for (pair<char, size_t> letter_with_count : remaining_paired_letters_with_counts) {
        char letter = letter_with_count.first;
        cout << letter;
    }
    // 5) put remaining letters from important and not important lists
    for (pair<char, size_t> letter_with_count : remaining_paired_letters_with_counts) {
        size_t count_in_middle = letter_with_count.second;
        char letter = letter_with_count.first;
        for (size_t i = 0; i < count_in_middle; i++) {
            cout << letter;
        }
    }
    for (char letter : single_letters) {
        cout << letter;
    }
    // 6) sink beginning in end of string
    auto sinked_letter_with_counts = remaining_paired_letters_with_counts.end();
    while (sinked_letter_with_counts != remaining_paired_letters_with_counts.begin()) {
        sinked_letter_with_counts--;
        cout << sinked_letter_with_counts->first;
    }
    return 0;
}