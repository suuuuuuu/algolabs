#include <cstdint>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void read_product_count_and_sale_number(uint64_t& product_count, uint64_t& sale_number) {
    uint64_t for_good_u8_read;
    cin >> product_count >> for_good_u8_read;
    sale_number = for_good_u8_read;
}

void read_products_prices(vector<uint64_t>& products_prices, uint64_t& count) {
    products_prices.reserve(count);
    for (uint64_t i = 0; i < count; i++) {
        uint64_t product_price;
        cin >> product_price;
        products_prices.push_back(product_price);
    }
}

int main() {
    vector<uint64_t> products_prices;
    uint64_t product_count;
    uint64_t sale_number;

    read_product_count_and_sale_number(product_count, sale_number);
    read_products_prices(products_prices, product_count);

    sort(products_prices.begin(), products_prices.end(), std::greater<uint64_t>());
    uint64_t sale_numbered_checks = product_count / sale_number;
    uint64_t pay_sum = 0;
    for (uint64_t check_index = 0; check_index < sale_numbered_checks; check_index++) {
        for (uint64_t product_index_to_pay_for = check_index * sale_number, i = 0;
                i < sale_number - 1;
                product_index_to_pay_for++, i++) {
            pay_sum += products_prices[product_index_to_pay_for];
        }
    }
    if (sale_numbered_checks * sale_number != product_count) {
        for (uint64_t product_index_to_pay_for = sale_numbered_checks * sale_number;
             product_index_to_pay_for < products_prices.size();
             product_index_to_pay_for++) {
            pay_sum += products_prices[product_index_to_pay_for];
        }
    }
    cout << pay_sum;
    return 0;
}