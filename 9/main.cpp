#include <iostream>
#include <vector>
#include <list>
#include <fstream>
#include <stack>
#include <queue>
#include <unordered_map>
#include <set>

using namespace std;

auto m_cmp = [](const pair<uint32_t, queue<uint32_t>*>& a, const pair<uint32_t, queue<uint32_t>*>& b) {
    if (a.second->empty()) {
        return false;
    } else if (b.second->empty()) {
        return true;
    } else {
        return a.second->front() > b.second->front();
    }
};

// returns vector of index = (car_number - 1), where value is car count
vector<uint32_t> read_data(list<uint32_t>& cars_wishlist, uint32_t& cars_count, uint32_t& floor_max_cars, istream& data_flow) {
    uint32_t wishlist_size;
    data_flow >> cars_count >> floor_max_cars >> wishlist_size;
    vector<uint32_t> cars_counts;
    cars_counts.resize(cars_count);
    for (uint32_t i = 0; i < wishlist_size; i++) {
        uint32_t wishlist_next_car_number;
        data_flow >> wishlist_next_car_number;
        cars_wishlist.push_back(wishlist_next_car_number);
        cars_counts[wishlist_next_car_number - 1]++;
    }
    return cars_counts;
}

void fill_eco_cars_with_wishlist_indexes(unordered_map<uint32_t, queue<uint32_t>>& eco_cars_with_wishlist_indexes,
                                         list<uint32_t>& cars_wishlist,
                                         vector<uint32_t>& cars_counts) {
    auto next_wishlist_car = cars_wishlist.begin();
    size_t wishlist_car_index = 0;
    while (next_wishlist_car != cars_wishlist.end()) {
        if (cars_counts[*next_wishlist_car - 1] > 1) {
            eco_cars_with_wishlist_indexes.emplace(*next_wishlist_car, queue<uint32_t>());
            eco_cars_with_wishlist_indexes[*next_wishlist_car].push(wishlist_car_index + 1);
        }
        next_wishlist_car++;
        wishlist_car_index++;
    }
}

uint32_t get_nearest_index(uint32_t& car,
                           unordered_map<uint32_t, queue<uint32_t>>& eco_cars_with_wishlist_indexes) {
    return eco_cars_with_wishlist_indexes[car].front();
}

void cout_eco_sorted_in_remove_order(list<uint32_t>& eco_sorted_in_remove_order) {
    auto i = eco_sorted_in_remove_order.begin();
    while (i != eco_sorted_in_remove_order.end()) {
        cout << *i;
        i++;
    }
    cout << "f" << endl;
}

bool is_eco_car(uint32_t& car, unordered_map<uint32_t, queue<uint32_t>>& eco_cars_with_wishlist_indexes) {
    return eco_cars_with_wishlist_indexes.find(car) != eco_cars_with_wishlist_indexes.end();
}

// 1) mark as on floor
// 2.1) it is not eco, it means it is not in eco_cars_with_wishlist_indexes map
// 2.1.1) add in non_eco_remove_queue
// 2.2) it is eco. it means it has key in eco_cars_with_wishlist_indexes
// 2.2.1) remove current index from eco_cars_with_wishlist_indexes
// 2.2.2) push to eco_sorted_in_remove_order
void move_car_on_floor(uint32_t car,
                      vector<bool>& is_on_floor_cars,
                      queue<uint32_t>& non_eco_remove_queue,
                      set<pair<uint32_t, queue<uint32_t>*>, decltype(m_cmp)>& eco_sorted_in_remove_order,
                      unordered_map<uint32_t, queue<uint32_t>>& eco_cars_with_wishlist_indexes) {
    if (!is_eco_car(car, eco_cars_with_wishlist_indexes)) {
        non_eco_remove_queue.push(car);
    } else {
        eco_cars_with_wishlist_indexes[car].pop();
        eco_sorted_in_remove_order.insert(pair(car, &eco_cars_with_wishlist_indexes[car]));
    }
    is_on_floor_cars[car - 1] = true;
}


// 0) unmark as on floor
// 1) if non_eco_remove_queue not empty
// 1.1) pop non_eco_remove_queue
// 2) if hasn't non_eco, we need to decide, which eco to remove
// 2.1) pop eco_sorted_in_remove_order
// 2.2) if eco_cars_with_wishlist_indexes with car key has only one member, it will be non-eco next time it will be pushed
// so remove eco_cars_with_wishlist_indexes
uint32_t remove_car_from_floor(vector<bool>& is_on_floor_cars,
                               queue<uint32_t>& non_eco_remove_queue,
                               set<pair<uint32_t, queue<uint32_t>*>, decltype(m_cmp)>& eco_sorted_in_remove_order,
                               unordered_map<uint32_t, queue<uint32_t>>& eco_cars_with_wishlist_indexes) {
    uint32_t removed_car;
    if (!non_eco_remove_queue.empty()) {
        removed_car = non_eco_remove_queue.back();
        non_eco_remove_queue.pop();
    } else {
        removed_car = eco_sorted_in_remove_order.begin()->first;
        eco_sorted_in_remove_order.erase(eco_sorted_in_remove_order.begin());
        if (eco_cars_with_wishlist_indexes[removed_car].size() <= 1) {
            eco_cars_with_wishlist_indexes.erase(removed_car);
        }
    }
    is_on_floor_cars[removed_car - 1] = false;
    return removed_car;
}
void cout_state(set<pair<uint32_t, queue<uint32_t>*>, decltype(m_cmp)>& eco_sorted_in_remove_order, queue<uint32_t>& non_eco_remove_queue) {
    auto i = eco_sorted_in_remove_order.begin();
    while (i != eco_sorted_in_remove_order.end()) {
        cout << i->first;
        i++;
    }
    cout << " ";
    cout << non_eco_remove_queue.size();
    cout << endl;
}
void remove_with_update_wishlist_index_of_eco(uint32_t car,
                                              queue<uint32_t>& non_eco_remove_queue,
                                              unordered_map<uint32_t, queue<uint32_t>>& eco_cars_with_wishlist_indexes,
                                              set<pair<uint32_t, queue<uint32_t>*>, decltype(m_cmp)>& eco_sorted_in_remove_order) {
    eco_sorted_in_remove_order.erase(pair(car, &eco_cars_with_wishlist_indexes[car]));
    eco_cars_with_wishlist_indexes[car].pop();
    if (eco_cars_with_wishlist_indexes[car].empty()) {
        eco_cars_with_wishlist_indexes.erase(car);
        non_eco_remove_queue.push(car);
    } else {
        eco_sorted_in_remove_order.insert(pair(car, &eco_cars_with_wishlist_indexes[car]));
    }
}


int main() {
    // data
    list<uint32_t> cars_wishlist;
    vector<uint32_t> cars_counts;
    set<pair<uint32_t, queue<uint32_t>*>, decltype(m_cmp)> eco_sorted_in_remove_order(m_cmp);
    queue<uint32_t> non_eco_remove_queue;
    unordered_map<uint32_t, queue<uint32_t>> eco_cars_with_wishlist_indexes;
    uint32_t cars_count, floor_max_cars;
    uint32_t operations_count = 0;

    ifstream data_flow("input.txt");
    cars_counts = read_data(cars_wishlist, cars_count, floor_max_cars, data_flow);
    data_flow.close();

    // 1) fill floor with cars, coming through wishlist
    // there is 2 ways: we will end algo here, because len(wishlist unique cars) = cars_count (<= or <) floor_max_cars
    //                  we will continue, but with removing, not only pushing cars
    // 2) come through wishlist till the end, doing remove-push work
    // inside if we have car on the floor, we need
    // 1) if it is eco. it means it has key in eco_cars_with_wishlist_indexes
    // 1.1) remove current index from eco_cars_with_wishlist_indexes and remove it from eco_sorted_in_remove_order
    // 1.1.1) if current index was last, remove him from eco_cars_with_wishlist_indexes
    // and add to non_eco_remove_queue
    // 1.1.2) if wasn't last (eco anyway), add it to eco_sorted_in_remove_order
    // 2) it is not eco, it means it is not in eco_cars_with_wishlist_indexes map
    // 2.1) don't need to perform any manipulations

    // push:
    // 1) mark as on floor
    // 2.1) it is not eco, it means it is not in eco_cars_with_wishlist_indexes map
    // 2.1.1) add in non_eco_remove_queue
    // 2.2) it is eco. it means it has key in eco_cars_with_wishlist_indexes
    // 2.2.1) remove current index from eco_cars_with_wishlist_indexes
    // 2.2.2) push to eco_sorted_in_remove_order
    // remove:
    // 0) unmark as on floor
    // 1) if non_eco_remove_queue not empty
    // 1.1) pop non_eco_remove_queue
    // 2) if hasn't non_eco, we need to decide, which eco to remove
    // 2.1) pop eco_sorted_in_remove_order
    // 2.2) if eco_sorted_in_remove_order has only one member, it will be non-eco next time it will be pushed
    // so remove eco_sorted_in_remove_order

    uint32_t left_to_fill_on_floor = floor_max_cars;
    fill_eco_cars_with_wishlist_indexes(eco_cars_with_wishlist_indexes, cars_wishlist, cars_counts);
    vector<bool> is_on_floor_cars;
    is_on_floor_cars.resize(cars_count);
    auto next_wishlist_car = cars_wishlist.begin();
    for (;next_wishlist_car != cars_wishlist.end() && left_to_fill_on_floor > 0;
            next_wishlist_car++) {
        bool is_on_floor = is_on_floor_cars[*next_wishlist_car - 1];
        if (is_on_floor) {
            remove_with_update_wishlist_index_of_eco(*next_wishlist_car, non_eco_remove_queue, eco_cars_with_wishlist_indexes, eco_sorted_in_remove_order);
            continue;
        }
        operations_count++;
        left_to_fill_on_floor--;
        move_car_on_floor(*next_wishlist_car,
                          is_on_floor_cars,
                          non_eco_remove_queue,
                          eco_sorted_in_remove_order,
                          eco_cars_with_wishlist_indexes);

    }
    for (;next_wishlist_car != cars_wishlist.end();
            next_wishlist_car++) {
        bool is_on_floor = is_on_floor_cars[*next_wishlist_car - 1];
        if (is_on_floor) {
            remove_with_update_wishlist_index_of_eco(*next_wishlist_car, non_eco_remove_queue, eco_cars_with_wishlist_indexes, eco_sorted_in_remove_order);
            continue;
        }
        operations_count++;
        remove_car_from_floor(is_on_floor_cars,
                              non_eco_remove_queue,
                              eco_sorted_in_remove_order,
                              eco_cars_with_wishlist_indexes);
        move_car_on_floor(*next_wishlist_car,
                          is_on_floor_cars,
                          non_eco_remove_queue,
                          eco_sorted_in_remove_order,
                          eco_cars_with_wishlist_indexes);
    }
    ofstream out_flow("output.txt");
    out_flow << operations_count;
    out_flow.close();
    return 0;
}